#!/bin/bash

UTIL_USERNAME="automation"
IDENTIFIER="${HOSTNAME}_automation"

# Add utility user to control machine (the one issuing automation) as a sudoer
sudo adduser "${UTIL_USERNAME}" --ingroup sudo --gecos ""
ssh-keygen -t ed25519 -C "${HOSTNAME}_automation" -f "${HOME}/.ssh/id_ed25519-${HOSTNAME}_automation" -N ''

# su automation
# ssh-copy-id -i "${HOME}/.ssh/id_ed25519-${HOSTNAME}_automation" "automation@target-hostname"

#cat "${HOME}/.ssh/id_ed25519-${HOSTNAME}_automation.pub" | \
#    ssh automation@192.168.1.9 \
#    "sudo mkdir -p /home/automation/.ssh; sudo tee -a /home/automation/.ssh/authorized_keys"

sshpass -p $1 ssh-copy-id -i ~/PATH/TO/KEY automation@192.168.1.9 -p $2