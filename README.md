# Homelab Bootstrapping

> Automated homelab configuration with [Ansible][href.ansible]

This project defines the desired state for each of the workstations and servers I run at home, providing
automation for:

- Installing and updating packages
- Non-[userland][href.user-space] config changes (outside of `$HOME`, e.g. things in [/etc][href.linux.etc] like
  [fstab][href.linux.fstab] entries) 
- Deploying [my dotfiles][href.gl.alexcochran.dotfiles] to set up apps and environments exactly how I like them

Ideally, this should allow for simple reconstitution of any machine that requires a reinstall of its operating
system. Once the base preferred GNU/Linux distribution is installed, one of the [playbooks](./ansible/playbooks)
can be run to bring the system to a state that maximizes usability.

<!-- Links -->
[href.ansible]: https://www.ansible.com/
[href.user-space]: https://en.wikipedia.org/wiki/User_space_and_kernel_space
[href.linux.etc]: https://tldp.org/LDP/sag/html/etc-fs.html
[href.linux.fstab]: https://wiki.archlinux.org/title/fstab
[href.gl.alexcochran.dotfiles]: https://gitlab.com/alexcochran/dotfiles
